extern crate sequoia_openpgp as openpgp;
use openpgp::{Packet};
use openpgp::packet::Signature::V4;
use openpgp::parse::{Parse, PacketParser, PacketParserResult};
use openpgp::packet::signature::subpacket::SubpacketValue::*;
use std::env;
use std::borrow::Cow;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let path = env::args().skip(1).next().unwrap();
    let mut ppr = PacketParser::from_file(path)?;

    while let PacketParserResult::Some(pp) = ppr {
        if let Packet::Signature(V4(ref sig)) = pp.packet {
            for sp in sig.hashed_area().iter() {
                if let NotationData(ref notation) = sp.value() {
                    println!("{:?} {} = {}", sig.issuer(), notation.name(), match std::str::from_utf8(&notation.value()) {
                        Ok(n) => Cow::Borrowed(n),
                        Err(_) => Cow::Owned(format!("{:x?}", notation.value()))
                    });
                }
            }
        }
        ppr = pp.recurse()?.1;
    }

    Ok(())
}
