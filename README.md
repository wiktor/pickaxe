# OpenPGP Pickaxe

Scans OpenPGP data for signature notations.

Example usage:

    cargo run data.gpg
